#include<pigpiod_if2.h>
#include<stdio.h>
#include<stdlib.h>
#include"common.h"

int pi;
int out_pin,in_pin;

//allocates memory and recieves a message to it
//returns bytes recieved
int rcv(char** dat){
	int recieved = 0;
	int good = 0;
	int sz;
	while(!good){
		sz = 0;
		recieved = 0;
		while(TRIM(recieved,8) != GOOD){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
		}	
		RECV_BYTES(pi,in_pin,sz,SIZE_BYTES,i)

		SEND_BYTES(pi,out_pin,BAD,1,i);
		SEND_BYTES(pi,out_pin,BAD,1,i);
		SEND_BYTES(pi,out_pin,GOOD,1,i);

		printf("%d\n",sz);
		SEND_BYTES(pi,out_pin,sz,SIZE_BYTES,i);
		gpio_write(pi,out_pin,0);
		recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}
		good = recieved == GOOD;
	}
	char* data = malloc(sz);
	int blocks = (sz+BLOCK_BYTES-1)/BLOCK_BYTES;
	int leftover = sz%BLOCK_BYTES?sz%BLOCK_BYTES:BLOCK_BYTES;
	for(int i = 0; i < blocks-1; i++){
		//printf("%d\n",i);
		good = 0;
		while(!good){
			recieved = 0;
			while(TRIM(recieved,8) != GOOD){
				recieved = (recieved<<1)| gpio_read(pi,in_pin);
				delay(RATE);
			}
			long long hash = 0;
			RECV_BYTES(pi,in_pin,hash,HASH_BYTES,j)
			//printf("%lld\n", hash);
			for(int j = 0; j < BLOCK_BYTES; j++){
				RECV_BYTES(pi,in_pin,data[j+BLOCK_BYTES*i],1,k)
			}
			if(hash != TRIM(get_hash(data+i*BLOCK_BYTES, BLOCK_BYTES),HASH_BITS)){
				SEND_BYTES(pi,out_pin,0,1,j);
				SEND_BYTES(pi,out_pin,BAD,1,j)
				gpio_write(pi,out_pin,0);
			}
			else{
				SEND_BYTES(pi,out_pin,0,1,j);
				SEND_BYTES(pi,out_pin,GOOD,1,j)
				gpio_write(pi,out_pin,0);
				good = 1;
			}
		}
	}
	good = 0;
	while(!good){
		recieved = 0;
		while(TRIM(recieved,8) != GOOD){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
		}
		long long hash = 0;
		RECV_BYTES(pi,in_pin,hash,HASH_BYTES,j)
		for(int j = 0; j < leftover; j++){
			RECV_BYTES(pi,in_pin,data[j+BLOCK_BYTES*(blocks-1)],1,k)
		}
		if(hash !=TRIM(get_hash(data+(blocks-1)*BLOCK_BYTES, leftover),HASH_BITS)){
			SEND_BYTES(pi,out_pin,0,1,j);
			SEND_BYTES(pi,out_pin,BAD,1,j)
			gpio_write(pi,out_pin,0);
		}
		else{
			SEND_BYTES(pi,out_pin,0,1,j);
			SEND_BYTES(pi,out_pin,GOOD,1,j)
			gpio_write(pi,out_pin,0);
			good = 1;
		}
	}

	SEND_BYTES(pi,out_pin,0,1,i)
	*dat = data;
	return sz;
}


int main(int argc, char** argv){
	//get parameters
	out_pin = atoi(argv[1]);
	in_pin = atoi(argv[2]);

	pi = setup(out_pin,in_pin);
	if(pi < 0) return 1;

	set_mode(pi,in_pin,PI_INPUT);
	char* data;
	int sz = rcv(&data);
	for(int i = 0; i < sz; i++)printf("%d ",(int)data[i]);
	printf("\n");
	free(data);

	end(pi);
	return 0;
}
