#include<semaphore.h>
#include<sys/ipc.h>
#include<unistd.h>

#ifndef COMMON_H
#define COMMON_H

#define TEST(n,i) (((n)&(1ll<<(i)))>>(i))
#define SET(n,i) ((n)|(1ll<<(i)))
#define TRIM(n,i) ((n)&((1ll<<(i))-1ll))

//microseconds per bit
//#define RATE 2500
#define RATE 10000

//transmitted size
#define SIZE_BYTES 2
#define SIZE_BITS (SIZE_BYTES*8)

//transmitted hash size
#define HASH_BYTES 2
#define HASH_BITS (HASH_BYTES*8)

#define ACTIVITY_BYTES 1
#define ACTIVITY_BITS (ACTIVITY_BYTES*8)

//base for hashing
#define HASH_BASE 331ll

//bytes in an error-checked block
#define BLOCK_BYTES 8

#define GOOD 0b10100101
#define BAD 0b11111111

#define SEND_BYTES(pi,pin,data,bytes,iter) for(int iter = bytes*8-1; iter >=0; iter--){gpio_write(pi,pin,TEST(data,iter));delay(RATE);}
#define RECV_BYTES(pi,pin,data,bytes,iter) for(int iter = 0; iter < bytes * 8; iter++){data = (data<<1)|gpio_read(pi,pin);delay(RATE);}

//wait length microseconds
//length < 1e6
void delay(long length);

//hashes sz bytes of data using HASH_BASE as a base
long long get_hash(char* data, int sz);

//return pigpiod port
int setup(int out_pin, int in_pin);

//stops connection to pigpiod
void end(int pi);


/*
 * for connecting to layer1
*/

/* message queue*/
#define MAX_Q 512
#define ACTIVITIES 512
typedef struct queueue{
	int sz; //size of contained message
	int msg; //contained message
	int activity; //activity of sender
	int next;
}queue_t;

typedef struct{
	sem_t sem;
	int q_size;

	int q_start;
	int q_end;
	
	int empty_start;
	int empty_end;
	
	char messages[MAX_Q][1<<SIZE_BITS];
	
	queue_t items[MAX_Q];
	
	int activities[ACTIVITIES];
	sem_t sems[ACTIVITIES];
	char recieved_messages[ACTIVITIES][(1<<SIZE_BITS)];
	int recieved_sizes[ACTIVITIES];
}communicator_t;

void queue_insert(communicator_t* com, int* start, int* end, int elem);
int queue_pop(communicator_t* com, int* start, int* end);

//connects to physical layer
communicator_t* com_connect(int activity, char* shm_string, int shm_number);
communicator_t* com_connect_default(int activity);
void com_disconnect(communicator_t* com, int activity);

//adds message to queue
int add_msg(communicator_t* com, int sz, void* msg, int activity);

#define SHM_STRING "LISA"
#define SHM_NUMBER 22
#define SHM_TOK ftok("LISA",22)

#endif
