#include<sys/shm.h>
#include<sys/ipc.h>
#include<pigpiod_if2.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include<signal.h>
#include<errno.h>
#include<semaphore.h>
#include"common.h"

int pi;
int out_pin,in_pin;

communicator_t* com;
int shmid;
char *shm_name_string;

int snd(char*data, int sz){
	int blocks = (sz+BLOCK_BYTES-1)/BLOCK_BYTES;
	int leftover = sz%BLOCK_BYTES?sz%BLOCK_BYTES:BLOCK_BYTES;
	//get hashes
	long long *hashes = malloc(blocks * sizeof(long long));
	for(int i = 0; i < blocks-1; i++){
		hashes[i] = get_hash(data + BLOCK_BYTES*i, BLOCK_BYTES);
	}
	hashes[blocks-1] = get_hash(data + BLOCK_BYTES*(blocks-1), leftover);


	//send size and error check it
	int good = 0;
	while(!good){
		SEND_BYTES(pi,out_pin,BAD,1,i)
		SEND_BYTES(pi,out_pin,BAD,1,i)
		//send start of transmission
		SEND_BYTES(pi,out_pin,GOOD,1,i)

		//send transmission size;
		SEND_BYTES(pi,out_pin,sz,SIZE_BYTES,i)
		gpio_write(pi,out_pin,0);	

		//get confirmation
		int recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}

		//get size
		int recieved_sz = 0;
		RECV_BYTES(pi,in_pin,recieved_sz,SIZE_BYTES,i)
		printf("%d\n",recieved_sz);
		good = sz == recieved_sz;
	}
	delay(RATE*2);

	SEND_BYTES(pi,out_pin,GOOD,1,i)

	for(int i = 0; i < blocks-1; i++){
		//printf("%d\n",i);
		good = 0;
		while(!good){
			SEND_BYTES(pi,out_pin,BAD,1,j)
			SEND_BYTES(pi,out_pin,BAD,1,j)
			SEND_BYTES(pi,out_pin,GOOD,1,j)
			SEND_BYTES(pi,out_pin,hashes[i],HASH_BYTES,j)
			//printf("%lld %d\n", TRIM(hashes[i],HASH_BITS),i);
			for(int j = 0; j < BLOCK_BYTES; j++){
				SEND_BYTES(pi,out_pin,data[j+BLOCK_BYTES*i],1,k)
			}
			int recieved = 0;
			int cycles = 0;
			while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
				recieved = (recieved<<1)| gpio_read(pi,in_pin);
				delay(RATE);
				cycles++;
			}
			//printf("%d\n",recieved);
			good = recieved == GOOD;
		}
		
	}
	good = 0;
	while(!good){
		SEND_BYTES(pi,out_pin,BAD,1,i)
		SEND_BYTES(pi,out_pin,GOOD,1,i)
		SEND_BYTES(pi,out_pin,hashes[blocks-1],HASH_BYTES,j)
		for(int j = 0; j < leftover; j++){
			SEND_BYTES(pi,out_pin,data[j+BLOCK_BYTES*(blocks-1)],1,k)
		}
		int recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}
		good = recieved == GOOD;
	}

	gpio_write(pi,out_pin, 0);
	free(hashes);
}

int rcv(char** dat){
	int recieved = 0;
	int good = 0;
	int sz;
	while(1){
		sz = 0;
		RECV_BYTES(pi,in_pin,sz,SIZE_BYTES,i)

		SEND_BYTES(pi,out_pin,BAD,1,i);
		SEND_BYTES(pi,out_pin,BAD,1,i);
		SEND_BYTES(pi,out_pin,GOOD,1,i);

		printf("%d\n",sz);
		SEND_BYTES(pi,out_pin,sz,SIZE_BYTES,i);
		gpio_write(pi,out_pin,0);
		recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}
		if(recieved == GOOD) break;
		recieved = 0;
		while(TRIM(recieved,8) != GOOD){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
		}	
	}
	good = 1;
	char* data = malloc(sz);
	int blocks = (sz+BLOCK_BYTES-1)/BLOCK_BYTES;
	int leftover = sz%BLOCK_BYTES?sz%BLOCK_BYTES:BLOCK_BYTES;
	for(int i = 0; i < blocks-1; i++){
		//printf("%d\n",i);
		good = 0;
		while(!good){
			recieved = 0;
			while(TRIM(recieved,8) != GOOD){
				recieved = (recieved<<1)| gpio_read(pi,in_pin);
				delay(RATE);
			}
			long long hash = 0;
			RECV_BYTES(pi,in_pin,hash,HASH_BYTES,j)
			//printf("%lld\n", hash);
			for(int j = 0; j < BLOCK_BYTES; j++){
				RECV_BYTES(pi,in_pin,data[j+BLOCK_BYTES*i],1,k)
			}
			if(hash != TRIM(get_hash(data+i*BLOCK_BYTES, BLOCK_BYTES),HASH_BITS)){
				SEND_BYTES(pi,out_pin,0,1,j);
				SEND_BYTES(pi,out_pin,BAD,1,j)
				gpio_write(pi,out_pin,0);
			}
			else{
				SEND_BYTES(pi,out_pin,0,1,j);
				SEND_BYTES(pi,out_pin,GOOD,1,j)
				gpio_write(pi,out_pin,0);
				good = 1;
			}
		}
	}
	good = 0;
	while(!good){
		recieved = 0;
		while(TRIM(recieved,8) != GOOD){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
		}
		long long hash = 0;
		RECV_BYTES(pi,in_pin,hash,HASH_BYTES,j)
		for(int j = 0; j < leftover; j++){
			RECV_BYTES(pi,in_pin,data[j+BLOCK_BYTES*(blocks-1)],1,k)
		}
		if(hash !=TRIM(get_hash(data+(blocks-1)*BLOCK_BYTES, leftover),HASH_BITS)){
			SEND_BYTES(pi,out_pin,0,1,j);
			SEND_BYTES(pi,out_pin,BAD,1,j)
			gpio_write(pi,out_pin,0);
		}
		else{
			SEND_BYTES(pi,out_pin,0,1,j);
			SEND_BYTES(pi,out_pin,GOOD,1,j)
			gpio_write(pi,out_pin,0);
			good = 1;
		}
	}

	SEND_BYTES(pi,out_pin,0,1,i)
	*dat = data;
	return sz;
}

void shm_setup(){
	shmid = shmget(ftok(shm_name_string,22),sizeof(communicator_t),0666|IPC_CREAT);
	shmctl(shmid, IPC_RMID, NULL);
	shmid = shmget(ftok(shm_name_string,22),sizeof(communicator_t),0666|IPC_CREAT);
	com = shmat(shmid, NULL,0);
	sem_init(&(com->sem), 1, 0);
	com->q_start = com->q_end = com->empty_start = com->empty_end = -1;
	for(int i = 0; i < MAX_Q; i++){
		queue_t* curr = &(com->items[i]);
		curr->sz = 0;
		curr->msg = i;
		curr->activity = 0;
		curr->next = -1;
		queue_insert(com,&(com->empty_start),&(com->empty_end), i);
	}
	for(int i = 0; i < ACTIVITIES; i++){
		sem_init(&(com->sems[i]), 1, 0);
	}
	memset(com->activities,0,sizeof(com->activities));
	sem_post(&(com->sem));
	printf("READY\n");
}

void shm_end(){
	sem_wait(&(com->sem));
	for(int i = 0; i < ACTIVITIES; i++){
		sem_destroy(&(com->sems[i]));
	}
	sem_destroy(&(com->sem));
	shmdt(com);
	shmctl(shmid, IPC_RMID, NULL);
}

void terminate(int foo){
	shm_end();
	end(pi);
	exit(0);
}

int main(int argc, char** argv){

	//gpio setup
	out_pin = atoi(argv[1]);
	in_pin = atoi(argv[2]);
	pi = setup(out_pin,in_pin);
	if(pi < 0) return 1;
	//end gpio setup
	
	shm_name_string = argv[3];
	shm_setup();

	signal(SIGINT, terminate);

	int item;
	while(1){
		item = -1;
		sem_wait(&(com->sem));
		if(com->q_size){
			item = queue_pop(com,&(com->q_start),&(com->q_end));
			com->q_size--;
		}
		sem_post(&(com->sem));
		if(item != -1){
			int sz = com->items[item].sz;
			void* msg = malloc(ACTIVITY_BYTES + sz);
			((int*)msg)[0] = com->items[item].activity;
			memcpy(msg+ACTIVITY_BYTES, com->messages[com->items[item].msg],sz);
			snd(msg, sz+ACTIVITY_BYTES);
			sem_wait(&(com->sem));
			queue_insert(com, &(com->empty_start), &(com->empty_end), item);
		}
		int cycles = 100;
		int recieved = 0;
		while(TRIM(recieved,8) != GOOD && ((cycles) || (TRIM(recieved,8)))){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles--;
		}
		if(TRIM(recieved,8) == GOOD){
			int sz;
			char* dat;
			sz = rcv(&dat);	
			int activity = TRIM(*(int*)dat, ACTIVITY_BITS);
			sem_wait(&(com->sem));
			com->recieved_sizes[activity] = sz - ACTIVITY_BYTES;
			memcpy(com->recieved_messages[activity], dat+ACTIVITY_BYTES, sz-ACTIVITY_BYTES);
			sem_post(&(com->sem));
			sem_post(&(com->sems[activity]));
		}
	}	

	end(pi);
	shm_end();
	return 0;
}
