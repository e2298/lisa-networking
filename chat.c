#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<signal.h>
#include<semaphore.h>
#include<string.h>
#include"common.h"
communicator_t* com;
pthread_t a,b;
int activity = 69;


void* reciever(void* nothing){
	while(1){
		sem_wait(&(com->sems[activity]));
		printf("Message:\n%s", com->recieved_messages[activity]);
	}
}

void* sender(void* nothing){
	while(1){
		char* msg = NULL;
		int sz = 0;
		getline(&msg, &sz, stdin);
		printf("sending %s\n", msg);
		sz = strlen(msg);
		sz++;
		printf("%d\n",add_msg(com, sz, msg, activity));	
		free(msg);
	}
}

void terminate(int foo){
	pthread_cancel(a);
	pthread_cancel(b);
	com_disconnect(com, activity);
	exit(0);
}

int main(int argc, char** argv){
	com = com_connect(activity, argv[1], SHM_NUMBER);
	printf("%p\n", com->items[com->empty_start].next);
	pthread_create(&a, NULL, reciever, NULL);
	pthread_create(&b, NULL, sender, NULL);
	
	signal(SIGINT, terminate);
	pthread_join(a,NULL);
	pthread_join(b,NULL);
	
	return 0;
}
