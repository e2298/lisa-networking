#include<sys/socket.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdint.h>
#include<signal.h>
#include<string.h>
#include<vector>
#include<set>
#include<thread>
#include<mutex>
#include<iostream>

//uncomment for access to light transfer
//#define LIGHT_HW
#ifdef LIGHT_HW
#include"common.h"
bool has_light = false;
communicator_t* com;
int activity = 69;
#endif

using namespace std;

#define PORT 42069
#define HASH_BASE 331ll
long long get_hash(const char* data, int sz){
	long long r = 0;
	for(int i = 0; i < sz; i++){
		r *= HASH_BASE;
		r += data[i]+1;
	}
	return r;
}


mutex print_mutex, send_mutex, seen_mutex, sockets_mutex;
vector<int> sockets;
set<long long> seen;
set<int> self_ids;


void relay(void* msg, int sz){
	send_mutex.lock();
	sockets_mutex.lock();
	for(auto socket:sockets){
		int netsz = htonl(sz);
		send(socket, &netsz, sizeof(int), 0);
		send(socket, msg, sz, 0);
	}
	#ifdef LIGHT_HW
	if(has_light){
		add_msg(com, sz, msg, activity);
	}
	#endif
	sockets_mutex.unlock();
	send_mutex.unlock();
}

void* get_messages(int idx){
	int socket = sockets[idx];
	while(1){
		int sz;
		recv(socket, &sz, sizeof(int), MSG_WAITALL);
		sz = ntohl(sz);
		if(sz == 0){
			print_mutex.lock();
			struct sockaddr_in addr;
			int len = sizeof(addr);
			getpeername(socket, (sockaddr*)&addr, (socklen_t*)&len);
			cout<<"Connection to "<<inet_ntoa(addr.sin_addr)<<" ended\n";
			print_mutex.unlock();
			return NULL;
		}
		char* buff = (char*)malloc(sz);
		recv(socket, buff, sz, MSG_WAITALL);
		long long hash = get_hash(buff, sz);
		seen_mutex.lock();
		if(seen.count(hash)){
			continue;
		}
		else{
			seen.insert(hash);
		}
		seen_mutex.unlock();

		int dest = *(int*)buff;
		dest = ntohl(dest);
		if(self_ids.count(dest)){
			print_mutex.lock();
			printf("Message recieved:\n%s\n", buff+sizeof(int));
			print_mutex.unlock();
		}
		else{
			print_mutex.lock();
			printf("Relaying message to %d\n", dest);
			print_mutex.unlock();
			relay(buff, sz);
		}
	}
}

void add_socket(int socket){
	sockets_mutex.lock();
	int idx = sockets.size();
	sockets.push_back(socket);
	sockets_mutex.unlock();
	new thread(get_messages, idx);
}


void* recieve_connections(){
	uint16_t port = PORT;
	port = htons(port);
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = port;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	memset(&(server_addr.sin_zero), 0, 8);

	int listening_socket = socket(PF_INET, SOCK_STREAM, 0);
	if(bind(listening_socket, (struct sockaddr*)&server_addr, sizeof server_addr)
	   || listen(listening_socket, 20)){
		print_mutex.lock();
		cout<<"Can't listen\n";
		print_mutex.unlock();
	}
	while(1){
		int new_socket = accept(listening_socket, NULL, NULL); 
		struct sockaddr_in addr;
		int len = sizeof(addr);
		getpeername(new_socket, (sockaddr*)&addr, (socklen_t*)&len);
		print_mutex.lock();
		cout<<"Recieved connection from:"<<inet_ntoa(addr.sin_addr)<<'\n';
		print_mutex.unlock();
		add_socket(new_socket);
	}
}

#ifdef LIGHT_HW
void* recieve_light(){
	while(1){
		sem_wait(&(com->sems[activity]));

		char* buff = (char*)malloc(com->recieved_sizes[activity]);
		memcpy(buff, com->recieved_messages[activity], com->recieved_sizes[activity]);
		int dest = *(int*)buff;
		dest = ntohl(dest);
		if(self_ids.count(dest)){
			print_mutex.lock();
			printf("Message recieved:\n%s\n", buff+sizeof(int));
			print_mutex.unlock();
		}
		else{
			print_mutex.lock();
			printf("Relaying message to %d\n", dest);
			print_mutex.unlock();
			relay(buff, com->recieved_sizes[activity]);
		}
	}
}

void terminate(int foo){
	if(has_light) com_disconnect(com, activity);
	exit(0);
}
#endif

int main(void){
	#ifdef LIGHT_HW
	signal(SIGINT, terminate);
	#endif
	new thread(recieve_connections);

	while(1){
		string command;
		cin>>command;
		if(command == "CONNECT"){
			string addr;
			cin>>addr;
			if(addr == "LIGHT"){
				#ifdef LIGHT_HW
				if(!has_light){
					com = com_connect_default(activity);
					if(com == NULL){
						print_mutex.lock();
						cout<<"Light connection not established\n";
						print_mutex.unlock();
					}
					else{
						new thread(recieve_light);
						has_light = true;
						print_mutex.lock();
						cout<<"Light connection established\n";
						print_mutex.unlock();
					}
				}
				else{
					print_mutex.lock();
					cout<<"Already connected\n";
					print_mutex.unlock();
				}
				#endif
				#ifndef LIGHT_HW
				print_mutex.lock();
				cout<<"Not compiled with light support\n";
				print_mutex.unlock();
				#endif
			}
			else{
				struct sockaddr_in target_addr;
				target_addr.sin_family = AF_INET;
				uint16_t port = htons(PORT);
				target_addr.sin_port = port;
				if(inet_pton(AF_INET, addr.c_str(), &(target_addr.sin_addr)) != 1){
					print_mutex.lock();
					cout<<"Invalid string\n";
					print_mutex.unlock();
				}
				memset(&(target_addr.sin_zero), 0, 8);
				int target_socket = socket(PF_INET, SOCK_STREAM, 0);
				if(connect(target_socket, (struct sockaddr*)&target_addr, sizeof(struct sockaddr_in))){
					print_mutex.lock();
					cout<<"Connection not established\n";
					print_mutex.unlock();
				}
				else{
					print_mutex.lock();
					add_socket(target_socket);
					cout<<"Connection established\n";
					print_mutex.unlock();
				}
			}
		}
		else if(command == "SEND"){
			int target;
			string msg;
			cin>>target;
			getline(cin,msg);
			char* buff = (char*)malloc(msg.size()+1+sizeof(int));
			*(int*)buff = htonl(target);
			memcpy(buff+sizeof(int), msg.c_str(), msg.size()+1);
			seen_mutex.lock();
			seen.insert(get_hash(msg.c_str(), msg.size()+1));
			seen_mutex.unlock();
			relay(buff, msg.size()+1 + sizeof(int));
		}
		else if(command == "NEW_ID"){
			int id;
			cin>>id;
			self_ids.insert(id);
		}
		else{
			print_mutex.lock();
			cout<<"Wrong command \n";
			print_mutex.unlock();
		}
	}
}
