#include<time.h>
#include<pigpiod_if2.h>
#include<stdlib.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<string.h>
#include<stdio.h>
#include"common.h"

//wait length microseconds
//length < 1e6
void delay(long length){
	long start;
	long diff;
	struct timespec now;
	length *= 1000;
	clock_gettime(CLOCK_REALTIME, &now);
	start = now.tv_nsec;
	while(1){
		clock_gettime(CLOCK_REALTIME, &now);
		diff = now.tv_nsec - start;
		if(diff < 0) diff += 1000000000;
		if(diff > length) break;

	}
}

//hashes sz bytes of data using HASH_BASE as a base
long long get_hash(char* data, int sz){
	long long r = 0;
	for(int i = 0; i < sz; i++){
		r *= HASH_BASE;
		r += data[i]+1;
	}
	return r;
}

//returns pigpiod port
int setup(int out_pin, int in_pin){
	int pi = pigpio_start(NULL,NULL);
	if(set_mode(pi,in_pin,PI_INPUT) || set_mode(pi,out_pin,PI_OUTPUT)) exit(1);
	return pi;
}

//stops connection to pigpiod
void end(int pi){
	return pigpio_stop(pi);
}

/*
 * for connecting to layer1
*/

// queue
void queue_insert(communicator_t* com, int* start, int* end, int elem){
	if(*start == -1){
		*start = *end = elem;
	}
	else{
		com->items[*end].next = elem;
	}
	com->items[elem].next = -1;
}

int queue_pop(communicator_t* com, int* start, int* end){
	int res = *start;
	*start = com->items[res].next;
	if(*start == -1) *end = -1;
	return res;
}

//shm
communicator_t* com_connect(int activity, char* shm_string, int shm_number){
	int shmid = shmget(ftok(shm_string,shm_number), sizeof(communicator_t), 0666);
	if(shmid == -1) return NULL;
	communicator_t* com = (communicator_t*)shmat(shmid, NULL, 0);
	sem_wait(&(com->sem));
	if(com->activities[activity]){
		shmdt((void*)com);
		return NULL;
	}
	com->activities[activity] = 1;
	sem_post(&(com->sem));
	return com;
}

communicator_t* com_connect_default(int activity){
	return com_connect(activity, SHM_STRING, SHM_NUMBER);
}
void com_disconnect(communicator_t* com, int activity){
	sem_wait(&(com->sem));
	com->activities[activity] = 0;
	sem_post(&(com->sem));
	shmdt((void*)com);
}

int add_msg(communicator_t* com, int sz, void* msg, int activity){
	sem_wait(&(com->sem));
	if(com->empty_start == -1) return 1;
	com->q_size++;
	int next_id = queue_pop(com, &(com->empty_start), &(com->empty_end));
	queue_t* nxt = &(com->items[next_id]);
	nxt->activity = activity;
	nxt->sz = sz;
	memcpy(com->messages[nxt->msg], msg, sz);
	queue_insert(com, &(com->q_start),&(com->q_end), next_id);
	sem_post(&(com->sem));
	printf("added %s to queue\n", msg);
	return 0;
}

