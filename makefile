OUTPUT = 2
INPUT = 3
SHM = LISA

send: send_exe 
	./send_exe $(OUTPUT) $(INPUT)

recv: recv_exe
	./recv_exe $(OUTPUT) $(INPUT)

serv: serv_exe
	./serv_exe $(OUTPUT) $(INPUT) $(SHM)

#simple chat
chat: chat_exe
	./chat_exe $(SHM)

#mesh chat
mesh: mesh_exe
	./mesh_exe

mesh_light: mesh_light_exe
	./mesh_light_exe

mesh_exe: mesh.cpp
	g++ -std=c++11 mesh.cpp -lpthread -o mesh_exe

mesh_light_exe: mesh.cpp common.c common.h
	g++ -std=c++11 -DLIGHT_HW mesh.cpp  common.c -lpthread -lpigpiod_if2 -lrt -o mesh_light_exe 

recv_exe: recv.c common
	gcc recv.c common.c -o recv_exe -lpigpiod_if2 -lrt -lpthread

send_exe: send.c common
	gcc send.c common.c -o send_exe -lpigpiod_if2 -lrt -lpthread

serv_exe: physical_layer.c common.c common.h
	gcc -pthread physical_layer.c common.c -o serv_exe -lpigpiod_if2 -lrt -lpthread

chat_exe: chat.c common.c common.h
	gcc -pthread chat.c common.c -o chat_exe -lpigpiod_if2 -lrt -lpthread

.PHONY: clean pigpiod
common: common.c common.h;

pigpiod: /var/run/pigpio.pid
	sudo pigpiod
