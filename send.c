#include<pigpiod_if2.h>
#include<stdio.h>
#include<stdlib.h>
#include"common.h"

int pi;
int out_pin,in_pin;

//send sz bytes from data
void snd(char* data, int sz){
	int blocks = (sz+BLOCK_BYTES-1)/BLOCK_BYTES;
	int leftover = sz%BLOCK_BYTES?sz%BLOCK_BYTES:BLOCK_BYTES;
	//get hashes
	long long *hashes = malloc(blocks * sizeof(long long));
	for(int i = 0; i < blocks-1; i++){
		hashes[i] = get_hash(data + BLOCK_BYTES*i, BLOCK_BYTES);
	}
	hashes[blocks-1] = get_hash(data + BLOCK_BYTES*(blocks-1), leftover);


	//send size and error check it
	int good = 0;
	while(!good){
		SEND_BYTES(pi,out_pin,BAD,1,i)
		SEND_BYTES(pi,out_pin,BAD,1,i)
		//send start of transmission
		SEND_BYTES(pi,out_pin,GOOD,1,i)

		//send transmission size;
		SEND_BYTES(pi,out_pin,sz,SIZE_BYTES,i)
		gpio_write(pi,out_pin,0);	

		//get confirmation
		int recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}

		//get size
		int recieved_sz = 0;
		RECV_BYTES(pi,in_pin,recieved_sz,SIZE_BYTES,i)
		printf("%d\n",recieved_sz);
		good = sz == recieved_sz;
	}
	delay(RATE*2);

	SEND_BYTES(pi,out_pin,GOOD,1,i)

	for(int i = 0; i < blocks-1; i++){
		//printf("%d\n",i);
		good = 0;
		while(!good){
			SEND_BYTES(pi,out_pin,BAD,1,j)
			SEND_BYTES(pi,out_pin,BAD,1,j)
			SEND_BYTES(pi,out_pin,GOOD,1,j)
			SEND_BYTES(pi,out_pin,hashes[i],HASH_BYTES,j)
			//printf("%lld %d\n", TRIM(hashes[i],HASH_BITS),i);
			for(int j = 0; j < BLOCK_BYTES; j++){
				SEND_BYTES(pi,out_pin,data[j+BLOCK_BYTES*i],1,k)
			}
			int recieved = 0;
			int cycles = 0;
			while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
				recieved = (recieved<<1)| gpio_read(pi,in_pin);
				delay(RATE);
				cycles++;
			}
			//printf("%d\n",recieved);
			good = recieved == GOOD;
		}
		
	}
	good = 0;
	while(!good){
		SEND_BYTES(pi,out_pin,BAD,1,i)
		SEND_BYTES(pi,out_pin,GOOD,1,i)
		SEND_BYTES(pi,out_pin,hashes[blocks-1],HASH_BYTES,j)
		for(int j = 0; j < leftover; j++){
			SEND_BYTES(pi,out_pin,data[j+BLOCK_BYTES*(blocks-1)],1,k)
		}
		int recieved = 0;
		int cycles = 0;
		while(TRIM(recieved,8) != GOOD && TRIM(recieved,8) != BAD && cycles < 100){
			recieved = (recieved<<1)| gpio_read(pi,in_pin);
			delay(RATE);
			cycles++;
		}
		good = recieved == GOOD;
	}

	free(hashes);
	SEND_BYTES(pi,out_pin,0,1,i)
}

int main(int argc, char** argv){
	//get parameters
	out_pin = atoi(argv[1]);
	in_pin = atoi(argv[2]);

	pi = setup(out_pin,in_pin);
	if(pi < 0) return 1;

	set_mode(pi,out_pin,PI_OUTPUT);

	char msg[] = " hm\n";
	msg[0] = 69;
	int sz = 5;
	snd(msg, sizeof(msg));

	end(pi);
	return 0;
}
